import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';

/**
 * Dependencies for the more complex table data
 */
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements DoCheck, OnInit {
    //define inputs
    list = [];
    @Output() itemsChange: EventEmitter<any> = new EventEmitter();
    @Input()
    get items() {
      return this.list;
    }

    set items(items) {
      this.list = items;
      this.itemsChange.emit(this.list);
    }

    @Input() page: number;
    @Input() maxpages: number;

    paginationlimit = 10;
    @Output() limitChange: EventEmitter<any> = new EventEmitter();
    @Input()
    get limit() {
      return this.paginationlimit;
    }

    set limit(val) {
      if (typeof val !== 'undefined') {
        this.paginationlimit = val;
        this.limitChange.emit(val);
        this.onChange.emit(this.page);
       }
    }

    @Output() onChange: EventEmitter<any> = new EventEmitter();
    @Input() fields: Array<any>;
    @Input() bulkOptions: Array<any>;
    @Output() bulkOptionChange:EventEmitter<any> = new EventEmitter();
    @Output() filter:EventEmitter<any> = new EventEmitter();
    @Input() actionButtons: Array<any> = [];
    @Output() emitAction:EventEmitter<any> = new EventEmitter();
    @Input() order: any = false;
    @Input() actionbar: any = true;
    @Input() sortabledirection: any = false;
    @Output() updateSortAction:EventEmitter<any> = new EventEmitter();
    @Output() detailToggle:EventEmitter<any> = new EventEmitter();
    @Output() clearSearchEmitter:EventEmitter<any> = new EventEmitter();

    constructor() { }

    allItems = false;

    displayedColumns = [];
    dataSource = new MatTableDataSource();

    ngDoCheck() {
        this.list.forEach((item) => {
            item.expand = true;
        })
        this.displayedColumns = [];
        // this.displayedColumns.push('expand');
        this.fields.forEach((field) => {
            this.displayedColumns.push(field.col);
        });
        this.dataSource.data = this.list;
    }

    pageChange(page) {
      this.onChange.emit(page);
    }

    updateBulk(action) {
      this.bulkOptionChange.emit(action);
    }

    clearSearchFunction() {
      this.clearSearchEmitter.emit();
    }

    selectItems(val) {
        this.allItems = val;
        // if it's true we have to go through all the items and check their boxes
        for(let i = 0; i < this.list.length; i++) {
            this.list[i].selected = this.allItems;
        }
    }

    clickAction(action) {
      this.emitAction.emit(action);
    }

    filterItems(query) {
      this.filter.emit(query);
    }

    updateListCheck(val, i) {
      this.list[i].selected = val;
    }

    updateSort(data) {
      this.updateSortAction.emit(data);
    }

    ngOnInit() {
    }

    orderDetail(order) {
      order.show = !order.show;
    }
}
