import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'listing-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
    //define inputs
    @Input() page: number;
    @Input() maxpages: number;

    showing = false;
    paginationlimit = 10;
    @Output() limitChange: EventEmitter<any> = new EventEmitter();
    @Input()
    get limit() {
        return this.paginationlimit;
    }

    set limit(val) {
        this.paginationlimit = val;
        this.limitChange.emit(val);
    }

    @Output() onChange: EventEmitter<any> = new EventEmitter();
    @Input() views: Array<any> = [5,10,20,50,100];

    constructor() { }

    ngOnInit() {
    }

    nextPage(page: number): void {
      if (page <= this.maxpages)
          page++;

      this.changePage(page);
    }

    prevPage(page: number): void {
      if (page >= 1)
          page--;

      this.changePage(page);
    }

    changePage(page: number): void {
      this.onChange.emit(page);
    }

    changeLimit(limit: number): void {
      this.limit = limit;
    }
}
