import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'action-select',
	templateUrl: './action-select.component.html',
	styleUrls: ['./action-select.component.css']
})
export class ActionSelectComponent implements OnInit {
	//define inputs
	@Input() options: Array<any>;
	@Input() row: object;
	@Output() onChange: EventEmitter<any> = new EventEmitter();

	constructor() { }

	ngOnInit() {
	}

	showing = false;

	processLink(link, replacements) {
		for(let key in replacements) {
			link = link.replace(':'+key, this.row[replacements[key]]);
		}
		return link;
	}

}
