import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// material components
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatDialogModule, MatSelectModule, MatCheckboxModule, MatRadioModule, MatTooltipModule, MatSlideToggleModule, MatProgressSpinnerModule, MatSidenavModule, MatIconModule, MatProgressBarModule, MatPaginatorModule, MatButtonModule, MatListModule, MatCardModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';

// router
import { AppRouterModule } from './app-router/app-router.module';

//services
import { ConnectionService } from './services/connection.service';
import { MinibcService } from './services/minibc.service';
import { InterceptorService } from './services/interceptor.service';
import { DialogService } from './common/notification/dialog/dialog.service';

// app sections
import { HomeComponent } from './views/home/home.component';
import { DialogComponent } from './common/notification/dialog/dialog.component';
import { DynamicFormComponent } from './common/forms/dynamic-form/dynamic-form.component';
import { ListingComponent } from './common/listing/listing.component';
import { ActionBarComponent } from './common/listing/action-bar/action-bar.component';
import { ActionSelectComponent } from './common/listing/action-select/action-select.component';
import { PaginationComponent } from './common/listing/pagination/pagination.component';
import { SortableComponent } from './common/listing/sortable/sortable.component';
import { OrderDetailComponent } from './views/order-detail/order-detail.component';
import { ProductListComponent } from './views/product-list/product-list.component';
import { ProductListItemComponent } from './views/product-list/product-list-item/product-list-item.component';
import { CheckboxComponent } from './common/input/checkbox/checkbox.component';
import { CaptureListComponent } from './views/capture-list/capture-list.component';
import { CaptureListItemComponent } from './views/capture-list/capture-list-item/capture-list-item.component';
import { InvoiceComponent } from './views/invoice/invoice.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DialogComponent,
    DynamicFormComponent,
    ListingComponent,
    ActionBarComponent,
    ActionSelectComponent,
    PaginationComponent,
    SortableComponent,
    OrderDetailComponent,
    ProductListComponent,
    ProductListItemComponent,
    CheckboxComponent,
    CaptureListComponent,
    CaptureListItemComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTableModule,
    CdkTableModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTooltipModule,
    MatDialogModule
  ],
  providers: [ConnectionService, DialogService, MinibcService, DialogService, { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true }],
  bootstrap: [AppComponent],
  entryComponents: [ InvoiceComponent, DialogComponent ],
})
export class AppModule { }
