import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'capture-list',
  templateUrl: './capture-list.component.html',
  styleUrls: ['./capture-list.component.css']
})
export class CaptureListComponent implements OnInit {

  constructor() { }

  @Input() order: any;
  @Input() shipping: any;
  @Input() capturedItems: any;
  @Input() discountPercentage: any;

  ngOnInit() {

  }

  getCaptureResult() {

  }

}
