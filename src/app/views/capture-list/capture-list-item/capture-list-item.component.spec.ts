import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptureListItemComponent } from './capture-list-item.component';

describe('CaptureListItemComponent', () => {
  let component: CaptureListItemComponent;
  let fixture: ComponentFixture<CaptureListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaptureListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptureListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
