import { Component, OnInit, Input } from '@angular/core';
import { ConnectionService } from '../../../services/connection.service';
import { DialogService } from '../../../common/notification/dialog/dialog.service';
import { InvoiceComponent } from '../../invoice/invoice.component';
declare var window: any;


@Component({
  selector: 'capture-list-item',
  templateUrl: './capture-list-item.component.html',
  styleUrls: ['./capture-list-item.component.scss']
})
export class CaptureListItemComponent implements OnInit {

  constructor(
    private connectionService: ConnectionService,
    private dialogService: DialogService
  ) { }

  @Input() product: any;
  @Input() order: any;
  @Input() shipping: any;
  storeInfo;
  hideInvoice = false;

  ngOnInit() {
    this.product.captured_value = parseFloat(this.product.captured_value).toFixed(2);
    this.getStoreInfo();
    if (this.product.status == 'Failed') this.hideInvoice = true;
  }

  getStoreInfo() {
    let captureListItem = this;
    // Grab store information
    let query = {
      'uri': '/v2/store'
    }

    let url = 'https://www.ribon.ca/api/live/' + window.store + '/get';

    this.connectionService.post(url, query).subscribe((res) => {
      captureListItem.storeInfo = res;
    });
  }

  printInvoice() {

    let data = {
      'storeInfo': this.storeInfo,
      'product': this.product,
      'order': this.order,
      'shipping':this.shipping,
    }

    this.dialogService.customDialog(InvoiceComponent, data);

  }

}
