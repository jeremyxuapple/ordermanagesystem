import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConnectionService } from '../../../services/connection.service';
declare var window: any;

@Component({
  selector: 'product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss']
})
export class ProductListItemComponent implements OnInit {

  constructor(
    private connectionService: ConnectionService
  ) { }

  @Input() product: any;
  @Input() discountPercentage: any;
  @Input() productStatus: any;
  @Output() selectProduct: EventEmitter<any> = new EventEmitter();
  @Output() updateCaptureQty: EventEmitter<any> = new EventEmitter();

  selectItem(matCheckBox) {

    let captureQty = this.product.captured ? this.product.qty_ordered : this.product.captureQty;

    let total = '';
    // if product is not shipping
    if (this.product.name != 'Shipping') {
      total = (parseFloat(this.product.sale_price) * parseFloat(captureQty)).toFixed(2);
    } else {
      total = (parseFloat(this.product.total_inc_tax) * parseFloat(captureQty)).toFixed(2);
    }

    let data =  {
      'productId':this.product.product_id,
      'selected': matCheckBox.checked,
      'orderId': this.product.order_id,
      'captureQty': captureQty,
      'total': total,
    }
    
    this.selectProduct.emit(data);
  }

  edit() {
    let url = 'https://store-' + window.store  + '.mybigcommerce.com/manage/orders/' + this.product.order_id +'/edit';
    let win = window.open(url, '_blank');
    win.focus();
  }

  ngOnInit() {

    this.product.captureQty = this.product.quantity;
    if (this.product.name == 'Shipping') {
      this.product.sale_price = parseFloat(this.product.base_price);
      this.product.total_inc_tax = parseFloat(this.product.base_price) * parseFloat(this.product.quantity);
    } else {
      this.product.sale_price = parseFloat(this.product.base_price) * parseFloat(this.discountPercentage);
      this.product.total_inc_tax = parseFloat(this.product.sale_price) * parseFloat(this.product.quantity);
    }

  }

  captureQtyChange($event) {

    if (this.product.selected) {
      let captureData = {
        'product_id': this.product.product_id,
        'captureQty': $event.target.value,
        'unit_price': this.product.sale_price
      }
      this.updateCaptureQty.emit(captureData);
    }

  }


}
