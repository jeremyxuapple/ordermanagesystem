import { Component, OnInit, Input, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import { ConnectionService } from '../../services/connection.service';
import { DialogService } from '../../common/notification/dialog/dialog.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
declare var window: any;

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class ProductListComponent implements OnInit {
  @Output() updateCapture: EventEmitter<any> = new EventEmitter();
  constructor(
    private connectionService: ConnectionService,
    private dialogService: DialogService,
  ) { }

	selectedProducts = [];
	allItems = false;
  captureDisplay = true;
  totalCaptureQty = 0.0;
  totalCaptureAmount = 0.0;

  // capture loading Indicator
  processing = false;

  @Input() products: any;
  @Input() customerId: number;
  @Input() orderStatus: any;
  @Input() actionBar: any;
  @Input() discountPercentage: any;
  @Input() shippingFees: any;
  @Input() orderId: any;
  @Input() capturedItems: any;

  ngOnInit() {

    let shipping = {
      'base_price': parseFloat(this.shippingFees).toFixed(2),
      'captureQty':1,
      'name':"Shipping",
      'order_id':this.orderId,
      'product_id':999999,
      'quantity':1,
      'sale_price':this.shippingFees,
      'selected':false,
      'sku':"SHIPPING",
      'total_inc_tax':this.shippingFees,
    }

    this.products.push(shipping);
  }


  updateCaptureQty($event) {
    
    this.totalCaptureQty = 0;
    this.totalCaptureAmount = 0;

    //update the selected product capture quantity
    this.selectedProducts.forEach((p) => {

      if (p.productId == $event.product_id) {
        p.captureQty = $event.captureQty;
        p.total = parseFloat($event.captureQty) * parseFloat($event.unit_price);
      }
    });

    this.selectedProducts.forEach((p) => {
      this.totalCaptureQty += parseInt(p.captureQty);
      this.totalCaptureAmount += parseFloat(p.total);
    });

  }

  selectProduct(product) {

    if (product.selected) {
      this.selectedProducts.push(product);
      // update the total quantity and total capture value
      this.totalCaptureQty += parseInt(product.captureQty);
      this.totalCaptureAmount += parseFloat(product.total);
    } else {
      // update the total quantity and total capture value
      this.totalCaptureQty -= parseInt(product.captureQty);
      this.totalCaptureAmount -= parseFloat(product.total);

      // filter out the unchecked product
      this.selectedProducts = this.selectedProducts.filter((prod) => {
        return prod.productId != product.productId
      }) ;
    }

  }

  captureSelcted() {
    this.processing = true;
    let url = '/ordermanagesystem/captureProducts';
    let token = window.minibc_token;
    let component = this;
    // console.log(this.selectedProducts);

		let data = {
			'customer_id': this.customerId,
			'products':this.selectedProducts,
      'storeID': window.storeId,
      'token': window.token,
		};

    this.connectionService.post(window.minibc_store + url, data).subscribe(captureResult => {
      // Error check
      if (captureResult.error) {
        component.dialogService.showDialogError('Gateway Malfunction', captureResult.error);
        this.processing = false;
        return;
      }

      var arr1 = [];
      var productsToSave = [];
      var formattedProducts = [];

      // Grabbing basic information of product for preparing pushing to the database
      this.selectedProducts.forEach( selectedProduct => {
        let dataToSave = this.products.filter((originalProduct) => {
            if (originalProduct.product_id == selectedProduct.productId) return true;
          });
          productsToSave.push(dataToSave[0]);
      });

      productsToSave.forEach((product) => {
        product['transactionId'] = captureResult.transactionId;
      });

      productsToSave.forEach((product) => {
        let transactionId = '';
        if (captureResult.payment_status === 'Success') transactionId = captureResult.gateway_data.card_type + ' - ' + captureResult.transactionId;
        let captureQty = product.captured ? product.qty_ordered : product.captureQty;

        let p = {
          'order_id': product.order_id,
          'product_id':product.product_id,
          'captured_value': (parseFloat(product.sale_price) * parseFloat(captureQty)).toFixed(2),
          'transaction_id': transactionId,
          'status':captureResult.payment_status,
          'unit_price':product.base_price,
          'error_msg': product.message,
          'quantity': captureQty,
          'name':product.name,
          'sku':product.sku
        }
        formattedProducts.push(p);
      });

      var updateBCOrderStatus = this.bigCommerceOrderStatusUpdateCheck(formattedProducts, this.capturedItems);

      if (updateBCOrderStatus) {
        let query = {
          'uri': '/v2/orders/' + this.products[0].order_id,
          "query": {
            'status_id':11
          }
        }
        let url = window.ribon + '/api/live/' + window.store +  '/put';

        this.connectionService.post(url, query).subscribe((res) => {

        });
      }

      this.postData(formattedProducts, false);

    });

  }

	selectItems(matCheckBox) {
    // update the product checked flag
    this.products.forEach((product) => {
      product.selected = matCheckBox.checked;
    });

    // update selected Product array
		if (!matCheckBox.checked) {
      this.totalCaptureQty = 0;
      this.totalCaptureAmount = 0;
      this.selectedProducts = [];
    } else {
      this.selectedProducts = [];
      this.totalCaptureQty = 0;
      this.totalCaptureAmount = 0;

      this.products.forEach((p) => {
        // update total capture quantity and capture
        let captureQty = p.captured ? p.qty_ordered : p.captureQty;
        let total = '';
        if (p.productId != 999999) {
          total = (parseFloat(p.sale_price) * parseFloat(captureQty)).toFixed(2);
        } else {
          total = p.total;
        }

        let prod = {
          'productId':p.product_id,
          'orderId': p.order_id,
          'total': total
        }

        this.totalCaptureQty += parseInt(captureQty);
        this.totalCaptureAmount += parseFloat(prod.total);

        this.selectedProducts.push(prod);
      });
    }

	}

  postData(products, processing) {
    let url = window.ribon_data + '/api/v1/data/oms_capture_items';
    let post = {
      'rows':products
    }

    this.connectionService.post(url, post).subscribe(response => {
      this.processing = processing;
      this.updateCapture.emit(true);

      // Clear up all the selected products
      this.products.forEach((p)=>{
        p.selected = false;
      });

      this.selectedProducts = [];
      this.totalCaptureQty = this.totalCaptureAmount = 0;

    });
  }

  manualCapture(productStatus) {
    if (productStatus == 'Paid Offline') {
      return;
    }

    this.processing = true;
    let arr = [];
    let manualCaptured = [];
    let status_id = 10;

    // Grabbing basic information of product for preparing pushing to the database
    this.selectedProducts.forEach( selectedProduct => {
      let dataToSave = this.products.filter((originalProduct) => {
          if (originalProduct.product_id == selectedProduct.productId) return true;
        });
        arr.push(dataToSave[0]);
    });

    arr.forEach((product) => {

      let p = {
        'order_id': product.order_id,
        'product_id':product.product_id,
        'captured_value': (parseFloat(product.sale_price) * parseFloat(product.qty_ordered)).toFixed(2),
        'transaction_id':'NA',
        'status':productStatus,
        'unit_price': product.sale_price,
        'error_msg':'NA',
        'quantity': product.qty_ordered,
        'name':product.name,
        'sku':product.sku
      }
      manualCaptured.push(p);
    });

    this.postData(manualCaptured, false);

    var updateBCOrderStatus = this.bigCommerceOrderStatusUpdateCheck(manualCaptured, this.capturedItems);

    // Update the order status
    if (!updateBCOrderStatus) {
      status_id = 3;
    }

    let query = {
      'uri': '/v2/orders/' + this.products[0].order_id,
      "query": {
        'status_id':status_id
      }
    }
    let url = window.ribon + '/api/live/' + window.store +  '/put';

    this.connectionService.post(url, query).subscribe((res) => {

    });
  }

  /**
  * Compare the capture this time and the items already captured before to verify if needs to update the
  * Big Commerce order status
  */

  bigCommerceOrderStatusUpdateCheck(capturedThisTime, capturedBefore) {

    var capturedItemUpdate = [ ];
    var productsOrdered = { };
    var updateBCOrderStatus = true;

    for(var i = 0; i < this.products.length; i++) {
      if (this.products[i].product_id == 999999 && this.products[i].sale_price == 0) {
        continue;
      }
      productsOrdered[this.products[i].product_id] = this.products[i].quantity;
    }

    capturedThisTime.forEach((product)=> {
      productsOrdered[product.product_id] -= 1;
    });

    capturedBefore.forEach((product)=> {
      productsOrdered[product.product_id] -= 1;
    });

    for (var key in productsOrdered) {
      if (productsOrdered[key] != 0) {
        // there is some products left without being captured
        updateBCOrderStatus = false;
      }
    }

    return updateBCOrderStatus;
  }

}
