import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../../services/connection.service';
declare var window: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private connectionService: ConnectionService
  ) { }

  ngOnInit() {
    this.getOrders();
  }

  bulkOptions = [
    'All Status', 'Pending', 'Awaiting Payment', 'Awaiting Fulfillment', 'Awaiting Shipment',
    'Completed', 'Shipped', 'Cancelled', 'Declined', "Partially Shipped",
    "Refunded", "Partially Refunded"
  ];

  // Progress spinner
  color = 'primary';
  mode = 'indeterminate';
  loading = true;

  dataAvailable = true;
	orders:any  = [];
  ordersToShow = [];
	page = 1;
	maxpages = 1;
  limit = 10;
	order = undefined;
	actionbar = false;
	sortdirection = 'asc';
	fields = [
    {name: 'Expand', col: 'expand', expand: true, type: true },
    {name: 'Order ID', col: 'id', basic: true },
		{name: 'Date', col: 'date', basic: true },
		{name: 'Customer Name', col: 'customer_name', basic: true },
    {name: 'Status', col: 'status', basic: true },
		{name: 'Total', col: 'total', basic: true },
    // {name: 'Sales Price', col: 'total', basic: true },
	];


  priciseRound(number, precision) {
    var shift = function (number, precision, reverseShift) {
      if (reverseShift) {
        precision = -precision;
      }
      var numArray = ("" + number).split("e");
      return +(numArray[0] + "e" + (numArray[1] ? (+numArray[1] + precision) : precision));
    };
    return shift(Math.round(shift(number, precision, false)), precision, true);
  }

  getOrders() {
    let query = {
      'uri': '/v2/orders'
    }
    let url = window.ribon + '/api/live/' + window.store +  '/get';

    this.connectionService.post(url, query).subscribe((res) => {
      if (!res.length)
        return false;

      // Filter out the Incomplete order:
      let orders = res.filter((o) => {
        return o.status != 'Incomplete';
      });


      orders.forEach((order) => {
        let discountPercentage = (parseFloat(order.total_inc_tax) - parseFloat(order.shipping_cost_inc_tax)) / parseFloat(order.subtotal_inc_tax);
        let roundedDiscount = this.priciseRound(discountPercentage, 4);

        let formattedOrder = {
          'id': order.id,
          'customer_id': order.customer_id,
          'status': order.status,
          'customer_name': order.billing_address.first_name + ' ' + order.billing_address.last_name,
          'email': order.billing_address.email,
          'phone': order.billing_address.phone,
          'currency_code': order.currency_code,
          'payment_provider_id': order.payment_provider_id,
          'company': order.billing_address.company,
          'city': order.billing_address.city,
          'shipping_cost_inc_tax': order.shipping_cost_inc_tax,
          'discountPercentage': roundedDiscount,
          'country': order.billing_address.country,
          'street_1': order.billing_address.street_1,
          'street_2': order.billing_address.street_2,
          'state': order.billing_address.state,
          'zip': order.billing_address.zip,
          'total': '$' + parseFloat(order.total_inc_tax).toFixed(2).toString(),
          'subtotal':'$' + (parseFloat(order.subtotal_inc_tax) + parseFloat(order.shipping_cost_inc_tax)).toFixed(2),
          'date': order.date_created.substring(5, 17),
          'show': false,
          'expand': true
        }

        this.orders.push(formattedOrder);

      }); // End of Order Format Loop

      // Sort the orders based on the order Id
      this.orders = this.orders.sort((a, b) => {
        return b.id - a.id;
      });

      // load the orders when page first load
      this.loading = false;
      this.changePage(1);
    });

  }

  changePage(pageNum) {
    this.dataAvailable = true;
    if (this.orders.length > 0) {
      this.maxpages = Math.round(this.orders.length / this.limit);
      this.page = pageNum;

      let startIndex = (this.page - 1) * this.limit;
      let endIndex = this.page * this.limit;
      this.ordersToShow = this.orders.slice(startIndex, endIndex);
    }

  }

  // Search function for customer name, order id and email address

  filterItems(query) {
    if (query == '') {
      this.ordersToShow = this.orders;
    } else {
      this.ordersToShow = [];
      this.dataAvailable = true;
      this.orders.forEach((order) => {
        let searchTarget = order.customer_name + order.email + order.id;
        if (searchTarget.indexOf(query) !== -1) this.ordersToShow.push(order);
      });

      if (this.ordersToShow.length == 0) this.dataAvailable = false;
    }
  }

  // Filter the order status, function name hasn't been update

  bulkChange(status) {

    if (status == 'All Status') {

      this.ordersToShow = this.orders;

    } else {
      this.ordersToShow = [];
      this.dataAvailable = true;
      this.orders.forEach((order) => {
        if (order.status == status) this.ordersToShow.push(order);
      });

      if (this.ordersToShow.length == 0) this.dataAvailable = false;
    }
  }

  clearSearchFunction() {
    this.bulkChange('All Status');
    this.filterItems('');
    this.changePage(1);

    // Reset input element
    let input = <HTMLInputElement>document.getElementById('searchInput');
    input.value = '';

    // Reset select options
    let select = <HTMLInputElement>document.getElementById('orderStatusSelect');
    select.value = 'All Status';

  }

}
