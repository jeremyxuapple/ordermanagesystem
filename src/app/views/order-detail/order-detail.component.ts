import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConnectionService } from '../../services/connection.service';
declare var window: any;

@Component({
  selector: 'order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  // Progress Indicator
  shippingLoading = true;
  productsLoading = true;
  captureLoading = true;
  blockActionBar = false;

  products = [];
  shipping: any = false;
  customerId = 0;
  capturedItems = [];
  orderStatus = '';
  orderId = 0;
  shippingFees = 0;

  @Input() order: any;
  // @Output() reloadCaptureList: EventEmitter<any> = new EventEmitter();

  constructor(
    private connectionService: ConnectionService
  ) { }

  ngOnInit() {
    this.orderId = this.order.id;
    this.shippingFees = this.order.shipping_cost_inc_tax;
    this.customerId = this.order.customer_id;
    this.orderStatus = this.order.status;
    this.getProducts();
    this.getCreditCardInfo();
    this.getShipping();
  }

  reloadCaptureList(e) {
    this.getData();
  }

  getProducts() {
   let query = {
     'uri': '/v2/orders/' + this.order.id + '/products'
   }
   let url = window.ribon + '/api/live/' + window.store +  '/get';

   this.connectionService.post(url, query).subscribe((res) => {

     res.forEach((product) => {
       let formattedProduct = {
         'quantity': product.quantity,
         'sku': product.sku,
         'base_price': parseFloat(product.base_price).toFixed(2),
         'total_inc_tax': parseFloat(product.total_inc_tax).toFixed(2),
         'name': product.name,
         'order_id': product.order_id,
         'product_id': product.product_id,
         'selected':false
       }
       this.products.push(formattedProduct);
     });

     this.productsLoading = false;
     // Get the captured Items
     this.getData();
   });
  }

  getShipping() {
    let orderDetail = this;
    let query = {
       'uri': '/v2/orders/' + this.order.id + '/shippingaddresses'
     }
    let url = window.ribon + '/api/live/' + window.store +  '/get';

    this.connectionService.post(url, query).subscribe((res) => {

      if (Array.isArray(res)) {
        orderDetail.shipping = res[0];
        orderDetail.shipping.cost_inc_tax = parseFloat(res[0].cost_inc_tax).toFixed(2);
      }
      this.shippingLoading = false;

    });
  }

  getData() {
    let url = window.ribon_data + '/api/v1/data/oms_capture_items?order_id=' + this.order.id;
    let headers = {
      'token': window.data_token
    }

    this.connectionService.get(url).subscribe(res => {
      this.capturedItems = res;

      if (res.length > 0 ) {

        // update qty_order column
        this.products.forEach((product) => {
          product.captured = true;
          product.qty_ordered = product.quantity;

          this.capturedItems.forEach((item) => {
            if (product.product_id == item.product_id && item.status != 'Failed') {
              // console.log(product.product_id);
              product.qty_ordered -= item.quantity;
            }
          });

      });

      }

      this.captureLoading = false;
    });
  }

  getCreditCardInfo() {
    // Make MiniBC request for pulling credit card information
    let minibc_url = '/ordermanagesystem/getCreditCardInfo';
    let data = {
      'storeID': window.storeId,
      'token': window.token,
      'orderId': this.order.id
    };

    this.connectionService.post(window.minibc_store + minibc_url, data).subscribe((res)=> {
      let creditCard = res;
      this.order.payment_method = creditCard.card_type;
      this.order.last4 = creditCard.last4;
      this.order.expiry = creditCard.expiry;
    });
  }
}
