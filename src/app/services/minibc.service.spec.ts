import { TestBed, inject } from '@angular/core/testing';

import { MinibcService } from './minibc.service';

describe('MinibcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MinibcService]
    });
  });

  it('should be created', inject([MinibcService], (service: MinibcService) => {
    expect(service).toBeTruthy();
  }));
});
