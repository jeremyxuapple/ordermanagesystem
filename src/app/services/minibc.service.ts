import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
declare var window: any;
declare var $: any;

@Injectable()
export class MinibcService {

    constructor() { }

    post(url: string, data: any): Promise<any> {
        if (typeof url === 'undefined')
            throw new Error("Please pass a URL to the ApiService");

        url = '/apps' + url;

        let query = new URLSearchParams(this.serialize(data, false));

        return window.MINIBC.request(data, url, 'POST', 'json');
    }

    get(url: string, query: object): Promise<any> {
        if (typeof query != 'undefined')
            url += '?' + this.serialize(query, false);

        if (typeof url === 'undefined')
            throw new Error("Please pass a URL to the ApiService");

        url = '/apps/' + window.app + url;

        return window.MINIBC.request(query, url, 'GET', 'json');
    }

    getContent(url, data?, method?): Promise<any> {
        if (!data)
            data = {};

        if (!method)
            method = 'POST';

        let object:any = {
            method: method,
            url: url,
            data: data,
            async: true,
        }

        return $.ajax(object);
    }

    public serialize(query: any, prefix: any): any {
        let str = [];
        for(let p in query) {
            if (query.hasOwnProperty(p)) {
                var k = prefix ? prefix + "[" + p + "]" : p, v = query[p];
                str.push((v !== null && typeof v === "object") ?
                    this.serialize(v, k) :
                    k + "=" + encodeURIComponent(v));
            }
        }

        return str.join("&");
    }

}
