import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Headers, RequestOptions, URLSearchParams, Http, RequestMethod, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ConnectionService {
    headers: any = false;
    constructor(
        private client: HttpClient,
        private http: Http,
    ) {
        this.headers = new HttpHeaders()
            .append('Content-Type', 'application/json');
    }

    get(url, head?): Observable<any> {
        return this.client.get(url);
    }

    post(url, query, file?): Observable<any> {
        let params = new HttpParams();

        params = this.parseParams(params, query, []);

        return this.client.post(url, params, this.headers);
    }

    public serialize(query: any, prefix: any): any {
        let str = [];
        for(let p in query) {
            if (query.hasOwnProperty(p)) {
                var k = prefix ? prefix + "[" + p + "]" : p, v = query[p];
                str.push((v !== null && typeof v === "object") ?
                    this.serialize(v, k) :
                    k + "=" + encodeURIComponent(v));
            }
        }

        return str.join("&");
    }

    private parseParams(params, query, depth) {
        for(let q in query) {
            if (typeof query[q] === 'object') {
                let newDepth = [];
                for(let d = 0; d < depth.length; d++) {
                    newDepth.push(depth[d]);
                }
                newDepth.push(q);
                params = this.parseParams(params, query[q], newDepth);
            } else {
                let s = q;
                for(let d = 0; d < depth.length; d++) {
                    if (d > 0) {
                        s += '['+depth[d]+']';
                    } else {
                        s = depth[d];
                    }

                    if (d === depth.length - 1) {
                        s += '['+q+']';
                    }
                }
                params = params.append(s, query[q]);
            }
        }

        return params;
    }

    private parseQuery(formData: any, query: any, depth: any) {
        for(let q in query) {
            if (typeof query[q] === 'object') {
                depth.push(q);
                formData = this.parseQuery(formData, query[q], depth);
            } else {
                let s = q;
                for(let d = 0; d < depth.length; d++) {
                    if (d > 0) {
                        s += '['+depth[d]+']';
                    } else {
                        s = depth[d];
                    }

                    if (d === depth.length - 1) {
                        s += '['+q+']';
                    }
                }

                formData.append(s, query[q]);
            }
        }

        return formData;
    }
}
