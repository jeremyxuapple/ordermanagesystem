import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
declare var window: any;

@Injectable()
export class InterceptorService implements HttpInterceptor {
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      if (req.url.indexOf(window.ribon_data) > -1) {
        // Append token for reques to save and get data through ribon database
          const dupReq = req.clone({ headers: req.headers.set('token', window.data_token) });
          return next.handle(dupReq);
      } else {
          return next.handle(req);
        }
    }


}
